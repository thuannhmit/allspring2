package com.spring.config.rabbitMQ;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Consumer {
    @RabbitListener(queues = {"${rabbitmq.queue}"})
    public void consume(@Payload String fileBody) {
        System.out.println("======================");
        System.out.println(fileBody);
        log.info(String.format("receive: %s", fileBody));
    }
}
